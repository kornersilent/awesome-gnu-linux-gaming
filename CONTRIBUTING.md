# Contributing

We are very open to contribution and we encourage it. There are several ways to contribute to this list:

## Filing an issue

Those that do not want to do the work can file an issue in [GitLab](https://gitlab.com/TheMainGroup/awesome-gnu-linux-gaming/-/issues) or in [GitHub](https://github.com/TheEvilSkeleton/awesome-gnu-linux-gaming/issues). While we do accept poorly written issues, we will appreciate if you spend your time writing an easy-to-read issue, for example:

```
Category:

AMD GPU

# Name:

ACO compiler

# Link(s):

https://wiki.archlinux.org/index.php/AMDGPU#ACO_compiler 

# Brief description:

Open source shader compiler by Valve Corporation to compete with the LLVM compiler, AMDVLK drivers drivers and Windows 10.
```

## Merging requests

If you want to pull merge requests then you are free to do so. The only requirement to merging requests is to do it in **GitLab**. Our [GitHub repository](https://github.com/TheEvilSkeleton/awesome-gnu-linux-gaming) is a mirror of our [GitLab repository](https://gitlab.com/TheMainGroup/awesome-gnu-linux-gaming).

Just do keep in mind that we will not accept all of the merge requests, some examples being troll merge requests and incomprehensible texts. We will appreciate if you put a relevant message in each commit, as well make your edits uniform with the our files so I (the maintainer) will not have to edit much.

We thank you in advance for contributing in GNU/Linux gaming!
